//Mi Clase en Java

public class HolaMundo {

    public static void main(String args[]) {
        //System.out.println ("Hola Mundo desde Java");

        //Definimos la variable
        //int miVariableEntera = 10;
        //System.out.println (miVariableEntera);
        
        //Modificamos el Valor de la Variab le
        //miVariableEntera = 5;
        //System.out.println(miVariableEntera);
        
        //String miVariableCadena = "Saludos";
        //System.out.println(miVariableCadena);
        
        //miVariableCadena = "Adios";
        //System.out.println(miVariableCadena);
        
        //A partir de la version 10 de Java se usa la paabra reservada var
        //var - inferencia de tipos en Java
        // var miVariableEntera2 = 15;
        //System.out.println(miVariableEntera2);
        //var miVariableCadena2 = "Nueva Cadena"
        
        //para imprimir la variable se digita
        //soutv y tabuladora y de inmediato me aparece en pantalla lo siguente:
        //System.out.println(miVariablecadena2 = " + miVariableCadena2);
        //En versiones anteriores a la 10 se debe digitar colocando el tipo de dato
        //String miVariableCadena2 = "Nueva Cadena"; 
        //System.out.println("miVariableCadena2 = " + miVariableCadena2);
        
        //Valores permitidos en el nombre de variables
        //var miVariable = 1;
        //var _miVariable = 2;
        //var $miVariable = 3;
        
        //Nota: no se pueden iniciar nombres de variables con numeros
        //áVariable = 10; no se recomienda utilizar acentos
        //#miVariable = 2; no se permite utilizar caracteres especiales
        
        String usuario = "Juan";
        String titulo = "Ingeniero";
        String union = titulo + " " + usuario;

        System.out.println("union = " + union);

    }
}
